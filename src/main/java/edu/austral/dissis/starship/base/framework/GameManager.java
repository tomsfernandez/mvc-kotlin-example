package edu.austral.dissis.starship.base.framework;

import edu.austral.dissis.starship.MainGameFramework;
import processing.core.PApplet;
import processing.event.KeyEvent;

import java.util.HashSet;
import java.util.Set;

public class GameManager extends PApplet {
    private final GameFramework gameFramework = new MainGameFramework();
    private final Set<Integer> keySet = new HashSet<>();

    public void settings() {
        gameFramework.setup(new WindowSettings(this).setSize(500, 500), new ImageLoader(this));
    }

    public void draw() {
        clear();

        final float timeSinceLastFrame = (frameRate / 60) * 100;
        gameFramework.draw(g, timeSinceLastFrame, keySet);
    }

    public void keyPressed(KeyEvent event) {
        keySet.add(event.getKeyCode());

        gameFramework.keyPressed(event);
    }

    public void keyReleased(KeyEvent event) {
        keySet.remove(event.getKeyCode());

        gameFramework.keyReleased(event);
    }
}
