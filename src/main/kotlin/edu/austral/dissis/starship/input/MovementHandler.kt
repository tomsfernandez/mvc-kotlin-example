package edu.austral.dissis.movable.input

import edu.austral.dissis.starship.domain.Movable
import edu.austral.dissis.starship.input.InputHandler
import edu.austral.dissis.starship.input.Key

class MovementHandler(private val movable: Movable) : InputHandler {
    override fun handle(key: Key) {
        when(key) {
            Key("LEFT") -> movable.rotate(-20)
            Key("UP") -> movable.forward()
            Key("RIGHT") -> movable.rotate(20)
            Key("DOWN") -> movable.backwards()
            else -> {} // ignore
        }
    }
}