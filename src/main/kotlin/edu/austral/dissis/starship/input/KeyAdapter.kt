package edu.austral.dissis.starship.input

import processing.core.PConstants
import processing.event.KeyEvent
import java.util.*

data class Key(val name: String)

class KeyAdapter {

    private val keyMap = mapOf(
            PConstants.LEFT to Key("LEFT"),
            PConstants.UP to Key("UP"),
            PConstants.RIGHT to Key("RIGHT"),
            PConstants.DOWN to Key("DOWN")
    )

    fun adapt(event: KeyEvent): Optional<Key> = Optional.ofNullable(keyMap[event.keyCode])
}