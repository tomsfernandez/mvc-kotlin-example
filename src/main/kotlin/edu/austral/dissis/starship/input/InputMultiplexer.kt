package edu.austral.dissis.starship.input

interface InputHandler {
    fun handle(key: Key)
}

class InputMultiplexer(private val handlers: Collection<InputHandler>) : InputHandler{

    override fun handle(key: Key) = handlers.forEach { h -> h.handle(key)}
}