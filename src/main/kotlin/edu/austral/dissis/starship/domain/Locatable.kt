package edu.austral.dissis.starship.domain

import edu.austral.dissis.starship.base.vector.Vector2

interface Locatable {

    fun dimensions(): TwoDimension
    fun direction(): Vector2
}