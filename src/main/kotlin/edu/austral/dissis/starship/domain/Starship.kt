package edu.austral.dissis.starship.domain

import edu.austral.dissis.starship.base.vector.Vector2
import java.awt.geom.Rectangle2D

class Starship : Movable, Locatable{

    private var shape: Rectangle2D = Rectangle2D.Float(100f, 100f, 50f, 50f)
    private var direction: Vector2 = Vector2.vectorFromModule(1f, 0f)

    override fun forward() {
        val newPositionToAdd = direction.multiply(15f)
        val bounds = shape.bounds
        bounds.setLocation((bounds.x + newPositionToAdd.x).toInt(), (bounds.y + newPositionToAdd.y).toInt())
        shape.setRect(bounds)
    }

    override fun rotate(degrees: Int) {
        direction = direction.rotate(Math.toRadians(degrees.toDouble()))
    }

    override fun backwards() {
        val newPositionToAdd = direction.multiply(-15f)
        val bounds = shape.bounds
        bounds.setLocation((shape.bounds.x + newPositionToAdd.x).toInt(), (shape.bounds.y + newPositionToAdd.y).toInt())
        shape.setRect(bounds)
    }

    override fun dimensions(): TwoDimension = TwoDimension.fromShape(shape)
    override fun direction(): Vector2 = direction
}
