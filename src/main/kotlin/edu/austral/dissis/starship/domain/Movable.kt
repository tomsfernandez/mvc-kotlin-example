package edu.austral.dissis.starship.domain

interface Movable {

    fun forward()
    fun rotate(degrees: Int)
    fun backwards()
}