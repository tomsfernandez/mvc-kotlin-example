package edu.austral.dissis.starship.domain

import java.awt.Shape

class TwoDimension(val xCenter: Float, val yCenter: Float, val width: Float, val height: Float) {

    companion object {
        fun fromShape(shape: Shape): TwoDimension {
            val shapeBounds = shape.bounds
            val xCenter = shapeBounds.width / -2 + shapeBounds.x
            val yCenter = shapeBounds.height / -2 + shapeBounds.y
            return TwoDimension(xCenter.toFloat(), yCenter.toFloat(), shapeBounds.width.toFloat(), shapeBounds.height.toFloat())
        }
    }
}