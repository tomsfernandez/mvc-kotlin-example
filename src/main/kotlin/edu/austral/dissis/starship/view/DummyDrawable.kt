package edu.austral.dissis.starship.view

import edu.austral.dissis.starship.domain.Locatable
import processing.core.PConstants
import processing.core.PGraphics


class DummyDrawable(private val locatable: Locatable) : Drawable{

    private val color: Color = Colors.ORANGE

    override fun draw(graphics: PGraphics) {
        val dimensions = locatable.dimensions()
        graphics.pushMatrix()
        graphics.translate(dimensions.xCenter, dimensions.yCenter)
        graphics.rotate(calculateRotation())
        graphics.fill(color.r, color.g, color.b)
        graphics.rect(-dimensions.width/2, -dimensions.height/2, dimensions.width, dimensions.height)
        graphics.popMatrix()
    }

    private fun calculateRotation(): Float {
        return locatable.direction().rotate((PConstants.PI / 2).toDouble()).angle
    }

}