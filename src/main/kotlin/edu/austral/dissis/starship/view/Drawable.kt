package edu.austral.dissis.starship.view

import processing.core.PGraphics

interface Drawable {

    fun draw(graphics: PGraphics)
}