package edu.austral.dissis.starship.view

object Colors {
    val ORANGE = Color(204f, 102f, 0f)
}

data class Color(val r: Float, val g: Float, val b: Float)