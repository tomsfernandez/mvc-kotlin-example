package edu.austral.dissis.starship

import edu.austral.dissis.movable.input.MovementHandler
import edu.austral.dissis.starship.base.framework.GameFramework
import edu.austral.dissis.starship.base.framework.ImageLoader
import edu.austral.dissis.starship.base.framework.WindowSettings
import edu.austral.dissis.starship.domain.Starship
import edu.austral.dissis.starship.input.InputHandler
import edu.austral.dissis.starship.input.InputMultiplexer
import edu.austral.dissis.starship.input.KeyAdapter
import edu.austral.dissis.starship.view.Drawable
import edu.austral.dissis.starship.view.DummyDrawable
import processing.core.PGraphics
import processing.event.KeyEvent

class MainGameFramework : GameFramework {

    private val starship: Starship = Starship()
    private val handler: InputHandler = InputMultiplexer(
            listOf(
                    MovementHandler(starship)
            )
    )
    private val drawable: Drawable = DummyDrawable(starship)
    private val adapter: KeyAdapter = KeyAdapter()

    override fun setup(windowsSettings: WindowSettings?, imageLoader: ImageLoader?) {
        // ignore
    }

    override fun draw(graphics: PGraphics, timeSinceLastDraw: Float, keySet: MutableSet<Int>?) {
        drawable.draw(graphics)
    }

    override fun keyPressed(event: KeyEvent) {
        val key = adapter.adapt(event)
        key.ifPresent { k -> handler.handle(k)}
    }

    override fun keyReleased(event: KeyEvent) {
        // ignored
    }
}